var net = require('net');
var dateTime = require('node-datetime');
var clients = [];
var request = require('request');
var issuesBefore = [];
var listUserNameFromSocket = [];
var ip = require('ip');
var fs = require('fs');
var nodemailer = require('nodemailer');


var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'huynhthehainam@gmail.com',
    pass: 'Doyouloveme'
  }
});

var mailOptions = {
  from: 'huynhthehainam@gmail.com',
  to: 'namhth@amitgroup.vn',
  subject: 'Sending Log file',
  text: 'Send log File',
  attachments: [{
    path: __dirname + '/logFile.json'
  }]
};

function sendLogFileToMail() {
  transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      console.log(err);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}



var statustable = [{
    "id": 2,
    "name": "In-progress"
  },
  {
    "id": 3,
    "name": "Open"
  },
  {
    "id": 4,
    "name": "Completed"
  },
  {
    "id": 5,
    "name": "New"
  },
  {
    "id": 7,
    "name": "Resolved"
  },
  {
    "id": 9,
    "name": "Blocked"
  }
];
console.log('Your IP address: ' + ip.address());
var listusers = [];

function getInitialUsers() {
  request({
    url: 'http://redmine.amitgroup.vn/users.json/?limit=100&key=3a028f45bf4496ff38e263e9fb6915d3acbb786c',
    json: true
  }, function (err, res, json) {
    if (err) {
      throw err;
    }
    if (isEmpty(listusers)) {
      for (var i in json.users) {
        user = {};
        user.username = json.users[i].login;
        user.computerName = json.users[i].lastname;
        user.id = json.users[i].id;
        user.workingtime = 0;
        user.isLogin = false;
        user.LoginTime = [];
        user.LogoutTime = [];
        user.listIssues = [];
        user.computerLog = [];
        listusers.push(user);
      }
    } else {

    }
  });
}

function writelog() {
  var dt = dateTime.create();
  var listUsertoWrite = JSON.parse(JSON.stringify(listusers));
  for (var i in listUsertoWrite) {
    for (var j in listUsertoWrite[i].LoginTime) {
      listUsertoWrite[i].LoginTime[j] = convertSectoHHMMString(listUsertoWrite[i].LoginTime[j]);
    }
    for (var j in listUsertoWrite[i].LogoutTime) {
      listUsertoWrite[i].LogoutTime[j] = convertSectoHHMMString(listUsertoWrite[i].LogoutTime[j]);
    }
    for (var j in listUsertoWrite[i].workingtime) {
      listUsertoWrite[i].workingtime[j] = convertSectoHHMMString(listUsertoWrite[i].workingtime[j]);
    }
  }
  var formatted = dt.format('Y-m-d');
  var rawData = fs.readFileSync('logFile.json');
  var previousLog = {};
  previousLog.log = [];
  try {
    previousLog = JSON.parse(rawData);
  } catch (e) {

  }
  var log = {};
  log.date = formatted;
  log.content = listUsertoWrite;
  previousLog.log.push(log);
  fs.writeFileSync('logFile.json', JSON.stringify(previousLog));
}

var listMonthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var endl = '\n';

function writeReport() {
  var rawData = fs.readFileSync('Report.csv').toString();
  var lineData = rawData.split(endl);
  var previousTable = [];
  for (var i = 3; i < lineData.length; i++) {
    previousTable.push(lineData[i].split(','));
  }
  var dt = dateTime.create();
  var fullday = dt.format('d-m-Y');
  var day = parseInt(dt.format('d'));
  var month = parseInt(dt.format('m'));
  var year = parseInt(dt.format('Y'));
  var contentToWriteFile = '';
  contentToWriteFile += 'Report in ' + listMonthName[month - 1] + endl;
  contentToWriteFile += ',';
  for (var i = 0; i < getDaysInMonth(month, year); i++) {
    contentToWriteFile += (i + 1).toString() + '-' + month.toString() + '-' + year.toString() + ',,';
  }
  contentToWriteFile += endl;
  contentToWriteFile += ',';
  for (var i = 0; i < getDaysInMonth(month, year); i++) {
    contentToWriteFile += 'Login,Logout,';
  }
  contentToWriteFile += endl;
  var table = [];
  for (var i in listusers) {
    var line = [];
    line[0] = listusers[i].username;
    if ((listusers[i].LoginTime.length != 0) && (listusers[i].LogoutTime.length != 0)) {
      line[2 * (day - 1) + 1] = convertSectoHHMMString(listusers[i].LoginTime[0]);
      line[2 * (day - 1) + 2] = convertSectoHHMMString(listusers[i].LogoutTime[listusers[i].LogoutTime.length - 1]);
    }
    for (var a in previousTable) {
      if (previousTable[a][0] == line[0]) {
        for (var b in previousTable[a]) {
          if (previousTable[a][b] != '' && previousTable[a][b] != '\n' && previousTable[a][b] != '\r') {
            line[b] = previousTable[a][b];
          }
        }
      }
    }
    table.push(line);
  }
  for (var i in table) {
    for (var j = 0; j < table[i].length; j++) {
      if (typeof table[i][j] != 'undefined') {
        contentToWriteFile += table[i][j] + ',';
      } else {
        contentToWriteFile += ',';
      }
    }
    contentToWriteFile += endl;
  }
  fs.writeFileSync('Report.csv', contentToWriteFile);

}

var getDaysInMonth = function (month, year) {
  return new Date(year, month, 0).getDate();
};

function resetWorkingTime() {
  var today = new Date();
  var time = today.getHours() * 3600 + today.getMinutes() * 60 + today.getSeconds();
  if (time == 86340) {
    writelog();
    writeReport();
    listusers = [];
    getInitialUsers();
  }
}

setInterval(resetWorkingTime, 1000);

function quickUpdateData(id, status, done_ratio) {
  var options = {
    url: 'http://redmine.amitgroup.vn/issues/' + id + '.json?key=3a028f45bf4496ff38e263e9fb6915d3acbb786c',
    method: 'PUT',
    json: {
      "issue": {
        "done_ratio": done_ratio,
        "status_id": status.id
      }
    }
  }
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {}
  });
}

function convertSectoHHMMString(second) {
  var HH = Math.floor(second / 3600);
  var MM = Math.floor((second % 3600) / 60);
  var HHMMString = HH.toString() + ':' + MM.toString();
  return HHMMString;
}


function countWorkingTime() {
  for (var i in listusers) {
    if (listusers[i].isLogin) {
      listusers[i].workingtime++;
    }
  }
}

setInterval(countWorkingTime, 1000);


function getUserNameFromIssuesId(id) {
  var username = '';
  if (id != null) {
    var index = getListFromJsonList(listusers, 'id').indexOf(id);
    if (index >= 0) {
      username = listusers[index].username;
    }
  }
  return username;
}

function getListFromJsonList(ListJson, field) {
  var list = [];
  for (var i in ListJson) {
    var listkey = Object.keys(ListJson[i]);
    if (listkey.indexOf(field) >= 0) {
      list.push(ListJson[i][field]);
    }
  }
  return list;
}


setInterval(resetWorkingTime, 1000);



function checkAPI() {
  request({
    url: 'http://redmine.amitgroup.vn/issues.json/?limit=100&key=3a028f45bf4496ff38e263e9fb6915d3acbb786c',
    json: true
  }, function (err, res, json) {
    if (err) {
      throw err;
    }
    if (isEmpty(issuesBefore)) {
      issuesBefore = json.issues;
    } else {
      var issues = json.issues;
      for (var i in issues) {
        var flag = false;
        for (var j in issuesBefore) {
          if (issues[i].id == issuesBefore[j].id) {
            if (issues[i].done_ratio != issuesBefore[j].done_ratio || issues[i].status_id != issuesBefore[j].status_id) {
              flag = true;
            }
          }
        }
        if (flag) {
          if (Object.keys(issues[i]).indexOf('assigned_to') >= 0) {
            var index = getListFromJsonList(listusers, 'id').indexOf(issues[i].assigned_to.id);
            if (index >= 0) {
              var userNamegetMessage = getListFromJsonList(listusers, 'username')[index];
              var index1 = listUserNameFromSocket.indexOf(userNamegetMessage);
              var emit = {};
              emit.type = 'messageFromServer';
              emit.content = '';
              emit.target = userNamegetMessage;
              if (index1 >= 0) {
                clients[index1].write(JSON.stringify(emit));
              }
            }
          }
        }
      }
      for (var i in issues) {
        var flag = false;
        for (var j in issuesBefore) {

          if (issues[i].id == issuesBefore[j].id) {
            flag = true;
          }
        }
        if (!flag) {
          if (Object.keys(issues[i]).indexOf('assigned_to') >= 0) {
            var index = getListFromJsonList(listusers, 'id').indexOf(issues[i].assigned_to.id);
            if (index >= 0) {
              var userNamegetMessage = getListFromJsonList(listusers, 'username')[index];
              var index1 = listUserNameFromSocket.indexOf(userNamegetMessage);
              var emit = {};
              emit.type = 'messageFromServer';
              emit.content = 'You got new issue';
              emit.target = userNamegetMessage;
              if (index1 >= 0) {
                clients[index1].write(JSON.stringify(emit));
              }
            }
          }
        }
      }
      for (var i in issuesBefore) {
        var flag = false;
        for (var j in issues) {
          if (issuesBefore[i].id == issues[j].id) {
            flag = true;
          }
        }
        if (!flag) {
          if (Object.keys(issuesBefore[i]).indexOf('assigned_to') >= 0) {
            var index = getListFromJsonList(listusers, 'id').indexOf(issuesBefore[i].assigned_to.id);
            if (index >= 0) {
              var userNamegetMessage = getListFromJsonList(listusers, 'username')[index];
              var index1 = listUserNameFromSocket.indexOf(userNamegetMessage);
              var emit = {};
              emit.type = 'messageFromServer';
              emit.content = 'You got out of a issue';
              emit.workingtime = listusers[index1].workingtime;
              emit.target = userNamegetMessage;
              if (index1 >= 0) {
                clients[index1].write(JSON.stringify(emit));
              }
            }
          }
        }
      }

      issuesBefore = issues;
    }
  });
}


checkAPI();
getInitialUsers();


setInterval(checkAPI, 3000);

function isEmpty(obj) {
  if (obj == null) return true;
  if (obj.length && obj.length > 0) return false;
  if (obj.length === 0) return true;
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

var server = net.createServer(function (socket) {
  socket.pipe(socket);
}).listen(8080);


function checkReview(index1) {
  _list = [];
  for (var j in issuesBefore) {
    if (Object.keys(issuesBefore[j]).indexOf('assigned_to') >= 0) {
      var _username = getUserNameFromIssuesId(issuesBefore[j].assigned_to.id)
      if (_username == listusers[index1].username) {
        _list.push(issuesBefore[j]);
      }
    }
  }
  var isReview = false;
  for (var j in _list) {
    for (var k in listusers[index1].listIssues) {
      if (_list[j].id == listusers[index1].listIssues[k].id) {
        if (_list[j].done_ratio != listusers[index1].listIssues[k].done_ratio || _list[j].estimated_hours != listusers[index1].listIssues[k].estimated_hours || _list[j].priority.id != listusers[index1].listIssues[k].priority.id || _list[j].status.id != listusers[index1].listIssues[k].status.id) {
          isReview = true;
        }
      }
    }
  }
  if (!isReview) {
    var emit = {};
    emit.type = 'feedBackReview';
    emit.content = 'No';
    emit.workingtime = listusers[index1].workingtime;
    emit.target = listusers[index1].username;
    return emit;

  } else {
    var emit = {};
    emit.type = 'feedBackReview';
    emit.content = 'Yes';
    emit.workingtime = listusers[index1].workingtime;
    emit.target = listusers[index1].username;
    return emit;
  }
}

server.on('error', function (err) {
  console.log(err.message);
});

server.on('connection', socket => {
  console.log('New client arrived');
  socket.on('error', function (err) {
    if (clients.includes(socket)) {
      var index = clients.indexOf(socket);
      if (index >= 0) {
        var listUserName = getListFromJsonList(listusers, 'username');
        var index1 = getListFromJsonList(listusers, 'username').indexOf(listUserNameFromSocket[index]);
        if (index1 >= 0) {
          listusers[index1].isLogin = false;
          var today = new Date();
          var time = today.getHours() * 3600 + today.getMinutes() * 60 + today.getSeconds();
          listusers[index1].LogoutTime.push(time);

        }
        clients.splice(index, 1);
        listUserNameFromSocket.splice(index, 1);
      }
    }
    console.log("Error: " + err.message);
  });
  socket.on('end', function () {
    console.log("Socket disconnected");
    if (clients.includes(socket)) {
      var index = clients.indexOf(socket);
      if (index >= 0) {
        var listUserName = getListFromJsonList(listusers, 'username');
        var index1 = getListFromJsonList(listusers, 'username').indexOf(listUserNameFromSocket[index]);
        if (index1 >= 0) {
          listusers[index1].isLogin = false;
          var today = new Date();
          var time = today.getHours() * 3600 + today.getMinutes() * 60 + today.getSeconds();
          listusers[index1].LogoutTime.push(time);

        }
        clients.splice(index, 1);
        listUserNameFromSocket.splice(index, 1);
      }
    }
  });

  socket.on('data', function (data) {
    console.log('Received: ' + data);
    var data1 = {};
    try {
      var data2 = String(data);
      data1 = JSON.parse(data2);
      if (Object.keys(data1).includes('computerName')) {
        data1.username = '';
        if (getListFromJsonList(listusers, 'computerName').indexOf(data1.computerName) >= 0) {
          data1.username = listusers[getListFromJsonList(listusers, 'computerName').indexOf(data1.computerName)].username;
        }
        console.log('User name: ' + data1.username);
      };
      if (data1.type == 'listen') {
        var index = getListFromJsonList(listusers, 'username').indexOf(data1.username);
        var index5 = listUserNameFromSocket.indexOf(data1.username);
        if (index >= 0 && index5 < 0) {
          _list = [];
          for (var j in issuesBefore) {
            if (Object.keys(issuesBefore[j]).indexOf('assigned_to') >= 0) {
              var _username = getUserNameFromIssuesId(issuesBefore[j].assigned_to.id)
              if (_username == data1.username) {
                _list.push(issuesBefore[j]);
              }
            }
          }
          listusers[index].listIssues = _list;
          clients.push(socket);
          listUserNameFromSocket.push(data1.username);
          var count = 0;
          for (var i in issuesBefore) {
            if (Object.keys(issuesBefore[i]).indexOf('assigned_to') >= 0) {
              var username = getUserNameFromIssuesId(issuesBefore[i].assigned_to.id);
              if ((username == data1.username) && (issuesBefore[i].done_ratio != 100) && (issuesBefore[i].status.name != 'Completed')) {
                count++;
              }
            }
          }
          var emit = {};
          emit.type = 'messageFromServer';
          emit.content = '';
          emit.target = data1.username;
          socket.write(JSON.stringify(emit));
          var listUserName = getListFromJsonList(listusers, 'username');
          var index1 = listUserName.indexOf(data1.username);
          if (index1 >= 0) {
            listusers[index1].isLogin = true;
            var today = new Date();
            var time = today.getHours() * 3600 + today.getMinutes() * 60 + today.getSeconds();
            listusers[index1].LoginTime.push(time);
          }
        } else {
          var emit = {};
          emit.type = 'preventConnecting';
          socket.write(JSON.stringify(emit));
        }
      } else if (data1.type == 'checkReview') {
        var index1 = getListFromJsonList(listusers, 'username').indexOf(data1.username);

        if (index1 >= 0) {
          listusers[index1].computerLog = data1.log;
          socket.write(JSON.stringify(checkReview(index1)));
        } else {
          var emit = {};
          emit.type = 'feedBackReview';
          emit.content = 'Yes';
          socket.write(JSON.stringify(emit));
        }
      } else if (data1.type == 'UpdateListUser') {
        if (data1.username == 'hainam') {
          listusers = [];
          getInitialUsers();
        }
      } else if (data1.type == 'writeLog') {
        if (data1.username == 'hainam') {
          writeReport();
        }
      } else if (data1.type == 'getIssuesData') {
        var index1 = getListFromJsonList(listusers, 'username').indexOf(data1.username);
        if (index1 >= 0) {
          var list = [];
          for (var i in issuesBefore) {
            if (Object.keys(issuesBefore[i]).includes('assigned_to')) {
              if (getUserNameFromIssuesId(issuesBefore[i].assigned_to.id) == data1.username) {
                list.push(issuesBefore[i]);
              }
            }
          }
          var emit = {};
          emit.type = 'feedBackData';
          emit.content = list;
          socket.write(JSON.stringify(emit));
        } else {
          var emit = {};
          emit.type = 'feedBackData';
          emit.content = [];
          socket.write(JSON.stringify(emit));
        }
      } else if (data1.type == 'updateData') {
        var status = statustable[getListFromJsonList(statustable, 'name').indexOf(data1.status)];
        quickUpdateData(data1.id, status, data1.done_ratio);
        var emit = {};
        emit.type = 'feedBackUpdate';
        socket.write(JSON.stringify(emit));
      } else if (data1.type == 'createData') {

      }
    } catch (e) {
      return;
    }
  });
});